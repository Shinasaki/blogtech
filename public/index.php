<?php

// Init Session
session_start();

// Init Environment
require_once('config/environment.php');

// hot path to upload
if (isset($_GET['rush']) && $_GET['rush'] == 'upload') {
  // if (isset($_FIELS["file"]["name"])) {
    require_once('./views/body/article/upload.php');
    die();
  // }
}

// Init Database
require_once('autoload/database.php');

// Init User
require_once('autoload/user.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Common Tag -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Search Engine -->
  <meta name="description" content="เว็บเขียนบทความ">
  <meta name="image" content="assets/images/favicon/icon-192x192.png">

  <!-- Prefetch -->
  <!-- <link rel=" dns-prefetch" href="//sn-curtain.com"> -->

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon//apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon//apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon//apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon//apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon//apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon//apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon//apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon//apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon//apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon//android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon//favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon//favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon//favicon-16x16.png">
  <link rel="manifest" href="assets/images/favicon//manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/images/favicon//ms-icon-144x144.png">
  <meta name="theme-color" content="#00171F">

  <!-- Import Font -->
  <!-- <link rel="stylesheet" href="assets/css/text/import.css"> -->
  <link href="https://fonts.googleapis.com/css?family=Mitr:300,400,500|Roboto:300,400,500" rel="stylesheet">

  <!-- Import Style -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.2.1/css/bootstrap-slider.min.css">
  <link rel="styleshhet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="assets/css/text/main.css">
  <link rel="stylesheet" href="assets/css/input/main.css">
  <link rel="stylesheet" href="assets/css/input/button.css">
  <link rel="stylesheet" href="assets/css/layout/dropdown.css">
  <link rel="stylesheet" href="assets/css/body/main.css">

  <!-- Import JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.2.1/bootstrap-slider.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.all.min.js"></script>
  

  <!-- Title -->
  <title>BLOGTECH</title>
</head>
  
<body>
  <!-- HEADER -->   
  <?php 
  require_once('views/header/header.php');
  ?>
  <!-- BODY -->
  <div class="body">
    <div class="container">
      <?php 
      require_once('autoload/router.php');
      ?>
    </div>
  </div>
  <!-- FOOTER -->
  <div class="footer">
    <div class="container">
      <?php
      require('views/body/footer.php');
      ?>
    </div>
  </div>
</body>
</html>