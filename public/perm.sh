#!/bin/sh
set -e

# Set directory permission
mkdir -p /var/www/html/assets/images/article
chmod -R 0755 /var/www/html/assets/images/article
chown -R www-data /var/www/html/assets/images/article

exec "$@"