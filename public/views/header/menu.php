<div class="dropdown">
  <i class="fas fa-bars"></i>
  <div class="dropdown-content">
    <a href="?route=home"> หน้าหลัก</a>
    <a href="?route=info"> เกี่ยวกับเรา</a>
    <a href="?route=create"> เขียนบทความ</a>
    <a href="?route=search"> ค้นหาบทความ</a>
  </div>
</div>
