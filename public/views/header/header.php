<link rel="stylesheet" type="text/css" href="assets/css/header/main.css">
<link rel="stylesheet" type="text/css" href="assets/css/header/brand.css">
<link rel="stylesheet" type="text/css" href="assets/css/header/search.css">
<link rel="stylesheet" type="text/css" href="assets/css/header/create.css">
<link rel="stylesheet" type="text/css" href="assets/css/header/menu.css">

<div class="header">
  <div class="navbar">
    <div class="row m-0">
      <div class="col-2 col-sm-2 col-md-3 col-lg-2 brand pr-0 pl-1">
        <div>
          <a href="?" class="pr-2">
            <img src="assets/images/brand.png">
          </a>
        </div>
        <div class="title d-none d-md-block">
          <a href="?">
            BLOCG TECH
          </a>
        </div>
      </div>
      <div class="col search pl-0">
        <form action="" method="GET">
          <input type="search" name="search" placeholder="ค้นหาบทความ">
          <input type="hidden" name="route" value="search">
          <button type="submit" class="fas fa-search submit-icon"></button>
        </form>
      </div>
      <div class="col-2 col-sm-2 col-md-1 col-lg-2 p-0 d-none d-sm-block create">
        <a href="?route=create">
          <button type="button">
            <i class="fas fa-plus"></i>
            <span class="d-none d-lg-inline">สร้างบทความ</span>
          </button>
        </a>
      </div>
      <div class="col-1 col-sm-1 col-md-1 col-lg-1 menu pl-0">
        <?php require_once('menu.php') ?>
      </div>
    </div>
  </div>
</div>