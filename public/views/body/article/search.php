<?php 

if (isset($_GET['search'])) {
  $search = mysqli_real_escape_string($connect, $_GET['search']);

  $query = "SELECT article.*, article_type.* FROM article article
  INNER JOIN (SELECT id as 'article_type_id', type_name, type_desc FROM article_type) article_type
  ON article.type_id=article_type.article_type_id
  WHERE (article.title LIKE '%$search%' OR article.content LIKE '%$search%' OR article_type.type_name LIKE '%$search%' OR article_type.type_desc LIKE '%$search%')  
  ";


  $result = mysqli_query($connect, $query);

  
} else {
  $query = "SELECT article.*, article_type.* FROM article article
  INNER JOIN (SELECT id as 'article_type_id', type_name, type_desc FROM article_type) article_type
  ON article.type_id=article_type.article_type_id
  ORDER BY RAND()
  ";
  $result = mysqli_query($connect, $query);
}
?>

<link rel="stylesheet" href="assets/css/body/home/list.css">
<div class="article-list-container mt-3 mb-3">
  <div class="title">
    ผลการค้นหา
  </div>
  <?php if ($result) {?>
    <?php while($items = mysqli_fetch_assoc($result)) { ?>
    <div class="row m-0 article-list-item">
      <div class="col-12 col-md-3 col-lg-2 list-image mr-3">
        <a href="?route=article&id=<?php echo $items['id']; ?>"><img class="w-100" src="<?php echo $items['cover']; ?>"></a>
      </div>
      <div class="col-12 col-md list-content">
        <div class="row m-0">
          <div class="col-12 list-title">
          <a href="?route=article&id=<?php echo $items['id']; ?>"><?php echo $items['title']; ?></a>
          </div>
          <div class="col-12 list-detail detail">
            <a href="?route=article&id=<?php echo $items['id']; ?>"><?php echo strip_tags($items['content']); ?></a>
          </div>
          <div class="col-12 list-navigator text-left detail mb-0">
            <span>ประเภท: </span> <span><a href="?route=search&search=<?php echo $items['type_name']; ?>"><?php echo $items['type_name']; ?></a></span>
            </div>
        </div>
      </div>
    </div>
    <?php } ?>
  <?php } else { ?>
    <div class="w-100 text-center">ไม่พบบทความ</div>
    <hr>
    <a href="?route=create">
      <button type="button" class="w-100 m-0">
        <i class="fas fa-plus"></i>
        <span class="d-none d-lg-inline">สร้างบทความ</span>
      </button>
    </a>
  <?php } ?>
</div>