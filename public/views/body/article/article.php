<?php 
if (isset($_GET['delete']) && isset($_GET['id'])) { // detect $_GET delete
  
  $article_id = mysqli_real_escape_string($connect, $_GET['id']);
  $query = "SELECT * FROM article WHERE id = '$article_id'";
  $content = mysqli_fetch_assoc(mysqli_query($connect, $query));

  if (!$content) { // check article is exist
    echo "<script>window.history.pushState('?')</script>";
    echo "<script>location.href = '?';</script>";
    die();
  }

  if ($content['user_id'] != constant("USER")) { // check user is own this article
    echo "<script>window.history.pushState('?route=article&id=$article_id')</script>";
    echo "<script>location.href = '?route=article&id=$article_id';</script>";
  }

  // delete article
  $query = "DELETE FROM article WHERE id = '$article_id'";
  $result = mysqli_query($connect, $query) or die("<script>ไม่สามารถลบบทความได้</script>");

  echo "<script>
    swal({ type: 'success', title: 'ลบบทความ', text: 'บทความของคุณได้ถูกลบแล้ว ระบบจะนำคุณไปหน้าหลัก'}).then(() => {
      location.href='?'
    })
  </script>";

  die();
}


if (isset($_GET['id'])) { // detect $_GET id

  // get article content
  $article_id = mysqli_real_escape_string($connect, $_GET['id']);
  $query = "SELECT * FROM article WHERE id='$article_id' LIMIT 1";
  $result = mysqli_query($connect, $query);

  
  if (mysqli_num_rows($result) <= 0) { // check article id is exist
    echo "<script>window.history.pushState('?')</script>";
    echo "<script>location.href = '?';</script>";
  }

  $content = mysqli_fetch_assoc($result);

  // update view count
  $query = "UPDATE article SET view_count = view_count + 1 WHERE id='$article_id'";
  mysqli_query($connect, $query);
  
} else {
  echo "<script>window.history.pushState('?')</script>";
  echo "<script>location.href = '?';</script>";
}
?>
<link rel="stylesheet" href="assets/css/body/article/article.css">
<div class="article">
  <div class="article-container">
    <div class="article-title">
      <?php echo $content['title']; ?>
      <hr>
    </div>
    <div class="row m-0">
      <div class="col article-nav-center">
        <div class="article-content">
          <?php echo $content['content']; ?>
        </div>
      </div>
    </div>
    <div class="article-footer mt-5">
      <div class="row m-0">
        <div class="col col-12">
          ถามเมื่อ: <?php echo explode(" ", $content['created_at'])[0] ?>
        </div>
        <div class="col col-12">
          จำนวนผู้อ่าน: <?php echo $content['view_count'] ?>
        </div>
        <div class="col col-12">
          โดยสมาชิก: <?php echo $content['user_id'] ?>
        </div>
      </div>
    </div>
    <?php if($content['user_id'] === constant("USER")) {?>
    <div class="row m-0 article-panel">
      <div class="col col-12 mt-3">
        <button type="button" onclick="swal({title: 'คุณต้องการลบบทความนี้หรือไม่?', type: 'warning', showCancelButton: true, confirmButtonText: 'ตกลง, ลบ', cancelButtonText: 'ยกเลิก, ไม่ลบ'}).then((result) => { if (result.value) location.href='?route=article&delete=true&id=<?php echo $content['id']; ?>' })">ลบบทความ <i class="fas fa-trash-alt"></i></button>
      </div>
    </div>
    <?php } ?>
  </div>
</div>