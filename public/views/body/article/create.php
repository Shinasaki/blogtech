<?php 
  if (isset($_POST['title']) && isset($_POST['article'])) {

    $title = mysqli_real_escape_string($connect, $_POST['title']);
    $article = mysqli_real_escape_string($connect, $_POST['article']);
    $user_id = mysqli_real_escape_string($connect, constant("USER"));
    $article_type = mysqli_real_escape_string($connect, $_POST['article_type']);

    // upload image to storage
    if ($_FILES["cover"]["tmp_name"]) {

      // create image name
      $name = $user_id . "-" . date("Ymdhi") . "-" . $_FILES["cover"]["name"];

      // create target directory file
      $target = "assets/images/article/" . $name;

      // move image to directory target
      move_uploaded_file($_FILES["cover"]["tmp_name"], $target);

      // insert database
      $query = "INSERT INTO article (title, content, cover, user_id, type_id) VALUES ('$title', '$article', '$target', '$user_id', '$article_type')";
      $result = mysqli_query($connect, $query);

      // get lastid
      $last_id = $connect->insert_id;

      echo "<script>
        swal({ type: 'success', title: 'สร้างบทความ', text: 'การสร้างบทความเสร็จสิ้นกด ตกลง เพื่อไปหน้าบทความของคุณ'}).then(() => {
          location.href='?route=article&id=$last_id'
        })
      </script>";
    }

  }

  // init article type
  $query = "SELECT * FROM article_type";
  $result = mysqli_query($connect, $query);
?>

<!-- script -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='assets/lib/froala/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<link href='assets/lib/froala/css/froala_style.min.css' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="assets/css/body/article/create.css">

<div class="create">
  <form method="POST" action='' id="article-create" enctype="multipart/form-data">
    <div class="row head m-0">
      <!-- <div class="col-3 col-sm-2 col-lg-1">
        <div class="title">TOPIC</div>
      </div> -->
      <div class="col">
        <input type="text" name="title" class="w-100" required placeholder="หัวข้อ">
      </div>
    </div>
    <div class="row m-0 article-area mt-3">
      <div class="col">
        <textarea class="w-100" id="froala-editor" name="article" placeholder="บทความของคุณ"></textarea>
      </div>
    </div>
    <div class="row m-0 mb-5">
      <div class="col-12 col-sm-6 col-md-5 sol-md-6">
        <table>
          <tr class="article-type">
            <td class="pt-3">
              <span>ประเภทบทความ</span>
            </td>
            <td class="pt-3">
              <select name="article_type" class="ml-2" required>
                <?php 
                while ($row = mysqli_fetch_assoc($result)) {
                echo "<option value='$row[id]'>$row[type_name]</option>";
                }
                ?>
              </select>
            </td>
          </tr>
          <tr class="article-upload">
            <td class="pt-3">
              <span>ภาพปก</span>
            </td>
            <td class="pt-3">
              <div class="btn-wrapper ml-2">
                <input type='file' name="cover" id="imageUpload" class="ml-2 button" accept=".png, .jpg, .jpeg" required/>
                <button type="button">อัพโหลด <i class="fas fa-upload"></i></button>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="col image-wrapper pt-3">
        <div id="imagePreview"></div>
      </div>
    </div>
    <div class="row m-0 bottom">
      <div class="col text-left d-none d-sm-inline">
        <button type="button" class="button" onclick="window.history.back();">
        <i class="fas fa-angle-left"></i>
          <span>
            ย้อนกลับ
          </span>
        </button>
      </div>
      <div class="col text-right">
        <button type="submit" class="button" id='save'>
          <span>
            สร้างบทความ
          </span>
          <i class="fas fa-plus-circle"></i>
        </button>
      </div>
    </div>
  </form>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
<script src="assets/lib/froala/js/froala_editor.pkgd.min.js"></script>

<script>

// onload
$(function() {

  // init froala editor
  $('textarea#froala-editor').froalaEditor({
    language: 'th',
    placeholderText: 'เริ่มเขียนบทความของคุณ...',
    imageUploadURL: '?rush=upload',
  });

})



function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          $('#imagePreview').css('background-image', 'url('+e.target.result +')');
          $('#imagePreview').hide();
          $('#imagePreview').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
  }
}


// upload image
$('#imageUpload').change(function() {
  readURL(this);
})
</script>

