<link rel="stylesheet" href="assets/css/body/footer.css">
<div class="footer">
  <div class="row m-0">
    <div class="col-12 p-0">
      <hr>
    </div>
    <div class="col-12 col-md p-0 pr-2">
      <div class="title">
        ABOUT <i class="fas fa-info"></i>
      </div>
      <div class="detail">
        คลิปพิซซ่าสไลด์ชัวร์ ฟินิกซ์แมชชีนคอลัมนิสต์ ซิตีเซ็นทรัลแกรนด์ซี้ เซ็นเซอร์ เวิร์กช็อปแจ๊กพ็อตอัลมอนด์โฟนอึ๋ม โยโย่คลิปคูลเลอร์แคนูโบตั๋น สตาร์ทเลิฟ บ๊วยเบนโตะ แจ็กเก็ตบ๊วย เคลียร์แบนเนอร์โมเต็ลเพียว แจมเอเซีย เบิร์ด อึ๋มแฟล็ตศิลปวัฒนธรรม เบญจมบพิตรสปารองรับเฝอสุริยยาตร ฮันนีมูน สลัมซิมโฟนี่
      </div>
      <div>
        BLOGTECH
        <hr class="d-block d-md-none">
      </div>
    </div>
    <div class="col p-0">
      <div class="slider">
        <?php 
          require($ENV_VAR['ROOT_DIR'] . '/views/body/home/slider.php');
        ?>
      </div>
    </div>
  </div>
</div>