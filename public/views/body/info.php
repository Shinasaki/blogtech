<link rel="stylesheet" href="assets/css/body/info.css">
<div class="all">
    <h2>ABOUT - เกี่ยวกับเรา</h2>
    <div class= "container-fluid">
        <div class= "container dev-profile">
            <div class="row">
                <img src= "assets/images/nut.jpg" width= 100px heigth= 100px>
                <div class= "box-info">
                    <i class="fab fa-facebook" id="face"></i>
                        <a href="https://www.facebook.com/nutorbitx">Nut Chukamphaeng</a><br>
                    <i class="fab fa-linkedin" id="linkedin"></i>
                        <a href="https://www.linkedin.com/in/nut-c-303606106/">Nut Chukamphaeng</a><br>
                    <i class="fab fa-instagram" id="insta"></i>
                        <a href="https://www.instagram.com/nutorbit/">nutorbit</a><br>
                    <i class="fas fa-at" id="mail"></i>
                        <a>60070134@it.kmitl.ac.th</a>
                </div>
            </div>

            <div class="row">
                <img src= "assets/images/sand.jpg" width= 100px heigth= 100px>
                <div class= "box-info">
                    <i class="fab fa-facebook" id="face"></i>
                        <a href="https://www.facebook.com/zand.nattanichaa">Sand Nattanicha</a><br>
                    <i class="fab fa-linkedin" id="linkedin"></i>
                        <a href="https://www.linkedin.com/in/nattanicha-chaisiripanich-0b624114a/">Nattanicha Chaisiripanich</a><br>
                    <i class="fab fa-instagram" id="insta"></i>
                        <a href="https://www.instagram.com/ntnchz/">Ntnchz</a><br>
                    <i class="fas fa-at" id="mail"></i>
                        <a>60070135@it.kmitl.ac.th</a>
                </div>
            </div>

            <div class="row">
                <img src= "assets/images/peach.png" width= 100px heigth= 100px>
                <div class= "box-info">
                    <i class="fab fa-facebook" id="face"></i>
                        <a href="https://www.facebook.com/PeachesTH">Vasin Sermsampan</a><br>
                    <i class="fab fa-linkedin" id="linkedin"></i>
                        <a href="https://www.linkedin.com/in/vasin-sermsampan/">Vasin Sermsampan</a><br>
                    <i class="fas fa-at" id="mail"></i>
                        <a>60070157@it.kmitl.ac.th</a>
                </div>
            </div>
        </div>

    </div>
</div>