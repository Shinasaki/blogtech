<link rel="stylesheet" href="assets/css/body/home/home.css">
<div class="home">

  <!-- Slider -->
  <div class="slider">
    <?php 
    require_once('slider.php');
    ?>
  </div>
  
  <div class="title mt-4">
    บทความ <i class="far fa-newspaper"></i>
  </div>

  <!-- Article list -->
  <div class="article-list">
    <?php 
    require_once('list.php');
    ?>
  </div>
  
</div>