<?php 
$query = "SELECT article.*, article_type.* FROM article article
INNER JOIN (SELECT id as 'article_type_id', type_name, type_desc FROM article_type) article_type
ON article.type_id=article_type.article_type_id
ORDER BY view_count DESC
LIMIT 7
";
$result = mysqli_query($connect, $query);
?>

<link rel="stylesheet" href="assets/css/body/home/slider.css">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php $count = 0; ?>
    <?php if ($result) {?>
      <?php while($items = mysqli_fetch_assoc($result)) { ?>
        <div class="carousel-item <?php if($count == 0) echo 'active'; ?>">
          <a href="?route=article&id=<?php echo $items['id']; ?>">
            <img class="d-block w-100" src="<?php echo $items['cover']; ?>" alt="<?php echo $items['title']; ?>">
            <div class="carousel-float">
              <div class="detail">
                <?php echo strip_tags($items['content']); ?>
              </div>
            </div>
          </a>
        </div>
      <?php $count += 1; }?>
    <?php } else { ?>
      <div class="carousel-item <?php if($count == 0) echo 'active'; ?>">
          <img class="d-block w-100" src="assets/images/slider/04.jpeg" alt="slider is empty">
          <div class="carousel-float">
            <div class="detail">
              <a href="?route=create">
                <button type="button" class="w-100 m-0">
                  <i class="fas fa-plus"></i>
                  <span class="d-none d-lg-inline">สร้างบทความ</span>
                </button>
              </a>
            </div>
          </div>
      </div>
    <?php } ?>
    
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">ย้อนกลับ</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">ต่อไป</span>
  </a>
</div>