<?php 

  if (!isset($_COOKIE['USER_COOKIE'])) {     // ถ้าไม่ได้ประกาศ USER_COOKIE ให้สร้าง cookie ใหม่ขึ้นมา
    setcookie('USER_COOKIE', session_id(), time()+60*60*24*100);     // cookie 1 ปี                       
  } else {
    define("USER", $_COOKIE['USER_COOKIE']);   // ประกาศต GLOBAL Variable USER โดยมีค่าใน cookie
  }


  // รับค่าจาก define โดยใช้คำสั่ง constant
  // เช่น echo constant("USER");
  
?>