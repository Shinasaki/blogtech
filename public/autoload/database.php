<?php
  $connect = mysqli_connect($ENV_VAR["MYSQL_HOST"], $ENV_VAR["MYSQL_USERNAME"], $ENV_VAR["MYSQL_PASSWORD"]);
  // echo $ENV_VAR["MYSQL_HOST"], $ENV_VAR["MYSQL_USERNAME"], $ENV_VAR["MYSQL_PASSWORD"], $ENV_VAR["MYSQL_DATABASE"], "<br>";
  if (!$connect) {
    die("<span style='color:red'>[ERROR] การเชื่อมต่อล้มเหลว ไม่สามารถเชื่อมต่อฐานข้อมูลได้. <br> กรุณาเช็ค config ที่ 'public/config/environment' ว่า username, password ถูกต้องหรือไม่. </span><br>" . mysqli_connect_error());
  }

  // check database is exist
  $query = "CREATE DATABASE IF NOT EXISTS $ENV_VAR[MYSQL_DATABASE] CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
  if(!mysqli_query($connect, $query)) {
    die("<span style='color:red'>[ERROR] ไม่สามารถสร้างฐานข้อมูล '$ENV_VAR[MYSQL_DATABASE]' ได้.</span><br>" . mysqli_error($connect));
  };

  // select database
  mysqli_select_db($connect, $ENV_VAR["MYSQL_DATABASE"]);
  

  // check table article type is exist
  // create table article
  $query = "CREATE TABLE IF NOT EXISTS `$ENV_VAR[MYSQL_DATABASE]`.`article_type` (
  `id` INT(30) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `type_name` VARCHAR(255) NOT NULL ,
  `type_desc` VARCHAR(255) NOT NULL
  ) ENGINE = InnoDB;";
  if (!mysqli_query($connect, $query)) {
    die("<span style='color:red'>[ERROR] ไม่สามารถสร้าง Table 'article_type' ได้.</span><br>" . mysqli_error($connect));
  }
  

  // check article_type is not empty
  $query = "SELECT * FROM `article_type`";
  $result = mysqli_query($connect, $query);
  if (mysqli_num_rows($result) <= 0) {
    // insert type of article_type
    $query = "INSERT INTO `article_type` (`id`, `type_name`, `type_desc`) VALUES (NULL, 'บทบรรณาธิการ', 'เป็นบทความแสดงความคิดเห็นลักษณะหนึ่งที่เขียนขึ้นเพื่อเสนอแนวคิดหลักของหนังสือพิมพ์ฉบับนั้นๆ ต่อเรื่องใดเรื่องหนึ่ง'), (NULL, 'บทความสัมภาษณ์', 'เป็นบทความที่เขียนขึ้นจากการสัมภาษณ์บุคคลเกี่ยวกับความคิดเห็นต่อเรื่องใดเรื่องหนึ่งหรือหลายเรื่อง'), (NULL, 'บทความแสดงความคิดเห็นทั่วๆไป', 'มีเนื้อหาหลายลักษณะ เช่น หยิบยกปัญหา เหตุการณ์ หรือเรื่องที่ประชาชนสนใจมาแสดงความคิดเห็น'), (NULL, 'บทความวิเคราะห์', 'เป็นบทความแสดงความคิดเห็นอย่างหนึ่ง ซึ่ง ผู้เขียนจะพิจารณาเรื่องใดเรื่องหนึ่งที่เผยแพร่มาแล้วอย่าง ละเอียด โดยแยกแยะให้เห็นส่วนต่างๆ ของเรื่องนั้น'), (NULL, 'บทความวิจารณ์', 'เขียนเพื่อแสดงความคิดเห็นในเชิงวิจารณ์ เรื่องราวที่ต้องการวิจารณ์ด้วยเหตุผลและหลักวิชาเป็นสำคัญ'), (NULL, 'บทความสารคดีท่องเที่ยว', 'มีเนื้อหาแนวบรรยาย เล่าเรื่องเกี่ยว กับสถานที่ท่องเที่ยวต่างๆ ที่มีทัศนียภาพสวยงาม'), (NULL, 'บทความกึ่งชีวประวัติ', 'เป็นการเขียนบางส่วนของชีวิตบุคคล เพื่อ ให้ผู้อ่านทราบ โดยเฉพาะคุณสมบัติ หรือผลงานเด่นที่ทำ ให้ บุคคลนั้นมีชื่อเสียง'), (NULL, 'บทความให้ความรู้ทั่วไป', 'ผู้เขียนจะอธิบายให้ความรู้คำแนะนำในเรื่องทั่วๆ ไปที่ใช้ในการดำเนินชีวิตประจำวัน'), (NULL, 'บทความเชิงธรรมะ', 'อธิบายข้อธรรมะให้ผู้อ่านทั่วๆ ไปเข้าใจได้ง่าย หรือให้คติ ให้แนวทางการดำเนินชีวิตตามแนวพุทธศาสนา'), (NULL, 'บทความวิชาการเนื้อหาแสดงข้อเท็จจริง', 'ข้อความรู้ทางวิชาการเรื่องใดเรื่องหนึ่ง ในสาขาวิชาใดวิชาหนึ่งโดยเฉพาะ')";
    if (!mysqli_query($connect, $query)) {
      die("<span style='color:red'>[ERROR] ไม่สามารถเพิ่มข้อมูลลงใน 'article_type' ได้.</span><br>" . mysqli_error($connect));
    }
  }

  $query = "CREATE TABLE IF NOT EXISTS $ENV_VAR[MYSQL_DATABASE].article(
    id int not null auto_increment primary key,
    title text not null,
    content text not null,
    cover text not null,
    view_count int not null default 0,
    like_count int not null default 0,
    user_id varchar(255) not null,
    type_id int(30) not null,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY fk_type(type_id)
    REFERENCES article_type(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
  )ENGINE=InnoDB;";
  if (!mysqli_query($connect, $query)) {
    die("<span style='color:red'>[ERROR] ไม่สามารถสร้าง Table 'article' ได้</span><br>" . mysqli_error($connect));
  }

  
  // // import article from article.sql
  // $filedir = $ENV_VAR['ROOT_DIR'] . '/assets/data/article.sql';
  // $file = file($filedir);
  // $templine = '';

  // // loop each line
  // foreach ($file as $line) {

  //   // Skip it if it's a comment
  //   if (substr($line, 0, 2) == '--' || $line == '')
  //   continue;

  //   // Add this line to the current segment
  //   $templine .= $line;

  //   if (substr(trim($line), -1, 1) == ';') {
  //     // Perform the query
  //     mysqli_query($connect, $templine); // Reset temp variable to empty
  //     $templine = '';
  //   }
  // }

?>