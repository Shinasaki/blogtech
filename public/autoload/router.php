<?php 
  // ระบบ routing
  if(isset($_GET['route'])) { // เช็คว่ามีค่า get ชื่อ route หรือไม่
    switch ($_GET['route']) { // switch case
      case 'home':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/home/home.php'); // ดึงหน้า home มาโดยอิงจาก root path ต่อด้วยที่อยู่
        break;
      case 'article':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/article/article.php');
        break;
      case 'create':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/article/create.php');
        break;
      case 'info':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/info.php');
        break;
      case 'search':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/article/search.php');
        break;
      case 'upload':
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/article/upload.php');
      default:
        require_once($ENV_VAR['ROOT_DIR'] . '/views/body/404.php');
        break;
    }
  } else { // ถ้าไม่เจอค่า get ชื่อ route ให้ดึงหน้า home มา
    // echo $ENV_VAR['ROOT_DIR'] . '/views/body/home/home.php'
    require_once($ENV_VAR['ROOT_DIR'] . '/views/body/home/home.php');
  }
?>