FROM php:7-fpm-alpine

RUN apk upgrade --update && apk --no-cache add zlib-dev \
  && docker-php-ext-install pdo pdo_mysql mysqli

RUN adduser root www-data

# RUN chown -R www-data /home/${LINUX_USERNAME}/www/blogtech-blog/html/html/assets/images/article
# RUN chmod -R 0755 /var/www/html/assets/images/article
